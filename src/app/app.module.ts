import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { UserComponent } from './components/user/user.component';

import { DataService } from './services/data.service';
import { HeaderComponent } from './components/header/header.component';
import { BannerComponent } from './components/banner/banner.component';
import { HomeComponent } from './components/home/home.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { SearchpageComponent } from './components/searchpage/searchpage.component';
import {ProductService} from "./services/product/product.service";
import { FooterComponent } from './components/footer/footer.component';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { ProductDetailsPageComponent } from './components/product-details-page/product-details-page.component';
import { AdminHomeComponent } from './admin/admin-home/admin-home.component';
import { AddCategoryComponent } from './admin/add-category/add-category.component';
import {CategoryService} from "./services/category/category.service";
import { AddBrandComponent } from './admin/brand/add-brand/add-brand.component';
import {BrandService} from "./services/brand/brand.service";
import { AddProductFeatureNamesComponent } from './admin/add-product-feature-names/add-product-feature-names.component';
import {ProductFeatureCategoryService} from "./services/productFeatureCategory/product-feature-category.service";
import {ProductFeatureNamesService} from "./services/ProductFeatureNames/product-feature-names.service";
import { AddProductComponent } from './admin/add-product/add-product.component';
import {ProductFeatureValueService} from "./services/ProductFeatureValue/product-feature-value.service";
import { EditProductFeatureNamesComponent } from './admin/edit-product-feature-names/edit-product-feature-names.component';
import { ManageProductComponent } from './admin/manage-product/manage-product.component';
import { EditProductComponent } from './admin/edit-product/edit-product.component';


const appRoutes : Routes = [
  {
    path : '',
    component : HomeComponent
  },
  {
    path : 'lp/:encodedId',
    component : LandingPageComponent
  },
  {
    path : 'search',
    component : SearchpageComponent
  },
  {
    path : 'product/:productId',
    component : ProductDetailsPageComponent
  },



  {
    path: 'admin',

    children: [
      {path: '', component: AdminHomeComponent},
      {path: 'add-category', component: AddCategoryComponent},
      {path: 'add-brand', component: AddBrandComponent},

      {path: 'add-product-feature-names', component: AddProductFeatureNamesComponent},
      {path: 'edit-product-feature-names', component: EditProductFeatureNamesComponent},

      {path: 'add-product', component: AddProductComponent},
      {path: 'manage-product', component: ManageProductComponent},
      {path: 'edit-product/:productId', component: EditProductComponent}
    ]
  }

];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HeaderComponent,
    BannerComponent,
    HomeComponent,
    LandingPageComponent,
    SearchpageComponent,
    FooterComponent,
    ProductCardComponent,
    ProductDetailsPageComponent,
    AdminHomeComponent,
    AddCategoryComponent,
    AddBrandComponent,
    AddProductFeatureNamesComponent,
    AddProductComponent,
    EditProductFeatureNamesComponent,
    ManageProductComponent,
    EditProductComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DataService,
              ProductService,
              CategoryService,
              BrandService,
              ProductFeatureCategoryService,
              ProductFeatureNamesService,
              ProductFeatureValueService
              ],
  bootstrap: [AppComponent]
})
export class AppModule { }
