import { Component, OnInit } from '@angular/core';
import {DataService} from "../../services/data.service";
import {Banner} from "../../entities/banner";

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  private banners : Banner[];

  constructor(private dataService : DataService) {
    this.dataService.getBanner().subscribe((banners) => {
        this.banners = banners;
        console.log(this.banners);
    });

    console.log(this.banners);
  }

  ngOnInit() {
  }

}


