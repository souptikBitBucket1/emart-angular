import { Injectable } from '@angular/core';
import {Http, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import {Utils} from "../../utils/utils";

@Injectable()
export class ProductService {

  private endpoint: string = Utils.emartBaseUrl + 'productApi/';
  constructor(private http: Http) { }

  getProducts(params : object){
    const options = new RequestOptions({params: params});
    return this.http.get(this.endpoint + 'search', options).map(res => res.json());
  }

  getProdcutDetails(productId: any){
    return this.http.get(this.endpoint + 'product/' + productId).map(res => res.json());
  }

  add(requestBody: object){
    return this.http.post(this.endpoint + 'add', requestBody).map(res => res.json());
  }

  getAll(){
    return this.http.get(this.endpoint + 'product/getAll').map(res => res.json());
  }

  update(productBody: object){
    return this.http.put(this.endpoint + 'product/' + productBody['id'], productBody).map(res => res.json());
  }

  deleteProduct(productId: number){
    return this.http.delete(this.endpoint + 'product/' + productId).map(res => res.json());
  }

  updateProductState(productId: number){
    return this.http.put(this.endpoint + 'product/updateState/' + productId, {}).map(res => res.json());
  }
}
