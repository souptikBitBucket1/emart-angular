import { Component, OnInit } from '@angular/core';
import {DataService} from "../../services/data.service";
import {Category} from "../../entities/category";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  category : Category;
  categories : Category[];

  constructor(private dataService : DataService) {

  }

  ngOnInit() {
    this.mapToCustomMenu();
  }

  mapToCustomMenu (){
    this.dataService.getCategories().subscribe((categories) => {
      this.categories = categories;
    });
  }

}

