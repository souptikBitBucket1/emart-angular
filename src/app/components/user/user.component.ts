import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import {post} from "selenium-webdriver/http";
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private dataService : DataService) {
    this.dataService.getCategories().subscribe((posts) => {
      console.log(posts);
    });
  }

  ngOnInit() {
  }

}
